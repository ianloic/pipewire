#!/bin/bash

set -euo pipefail

git pull --quiet

HASH=$(git rev-parse --short upstream/master)
VER=$(grep 'version : ' meson.build|head -1|cut -d "'" -f 2)
TS=$(date --utc '+%Y%m%d%H%M%S')

debchange --newversion "$VER-git-$TS-$HASH" "Updated sources"
debchange --release
